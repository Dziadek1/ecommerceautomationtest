package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobjects.*;

public class Navigation {

    public static BasePage goToBasePage(WebDriver driver, WebDriverWait wait) {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        return basePage;
    }

    public static LoginPage goToLoginPage(WebDriver driver, WebDriverWait wait) {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        LoginPage loginPage = homePage.goToLogin();
        return loginPage;
    }

    public static ForgotPasswordPage goToForgotPasswordPage(WebDriver driver, WebDriverWait wait) {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        LoginPage loginPage = homePage.goToLogin();
        ForgotPasswordPage forgotPasswordPage = loginPage.goToForgotPasswordPage();
        return forgotPasswordPage;
    }
    public static MyAccountPage goMyAccountPage(WebDriver driver, WebDriverWait wait) {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("Testludmila@test.pl", "Testludmila@test.pl");
        MyAccountPage myAccountPage = loginPage.goMyAccount();
        return myAccountPage ;
    }

    public static MyAddressesPage goToAddressesPage(WebDriver driver, WebDriverWait wait) {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("Testludmila@test.pl", "Testludmila@test.pl");
        MyAccountPage myAccountPage = loginPage.goMyAccount();
        MyAddressesPage myAddressesPage = myAccountPage.goToMyAddressesPage();
        return myAddressesPage;
    }

    public static ChangeAddressesPage goToChangeAddressesPage(WebDriver driver, WebDriverWait wait) {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("Testludmila@test.pl", "Testludmila@test.pl");
        MyAccountPage myAccountPage = loginPage.goMyAccount();
        MyAddressesPage myAddressesPage = myAccountPage.goToMyAddressesPage();
        ChangeAddressesPage changeAddressesPage = myAddressesPage.goToUpdateAddressesPage();
        return changeAddressesPage;
    }

}
