package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyAccountPage extends BasePage {

    public MyAccountPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(xpath = "//a[@title = 'Addresses']")
    private WebElement myAddressesButton;

    @FindBy(className = "icon-list-ol" )
    private WebElement orderHistoryAndDetailsButton;


    public MyAddressesPage goToMyAddressesPage() {
        wait.until(ExpectedConditions.elementToBeClickable(myAddressesButton));
        myAddressesButton.click();
        return new MyAddressesPage(driver, wait);
    }

    public MyAddressesPage goToOrderHistoryAndDetailsPage() {
        wait.until(ExpectedConditions.elementToBeClickable(orderHistoryAndDetailsButton));
        orderHistoryAndDetailsButton.click();
        return new MyAddressesPage(driver, wait);
    }

    public String getTitle() {
        String title = driver.getTitle();
        return title;
    }
}
