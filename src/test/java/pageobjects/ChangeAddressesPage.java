package pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.RandomUser;

import java.util.List;

public class ChangeAddressesPage extends BasePage{
    public ChangeAddressesPage(WebDriver driver, WebDriverWait wait) {super(driver, wait);
    }

    @FindBy(id = "firstname")
    private WebElement firstName;

    @FindBy(id = "lastname")
    private WebElement lastName;

    @FindBy(id = "company")
    private WebElement company;

     @FindBy(id = "address1")
    private WebElement address1;

    @FindBy(id = "address2")
    private WebElement address2;

    @FindBy(id = "city")
    private WebElement city;

    @FindBy(xpath = "//div[@class='alert alert-danger']//p[1]")
    private WebElement changeAddressAlertMainText;

    @FindBy(xpath = "//div[@class='alert alert-danger']//ol")
    private WebElement changeAddressAlertDetailsText;

    @FindBy(id = "submitAddress")
    private WebElement submitAddressButton;

    @FindBy(id = "id_state")
    private WebElement id_state;

    @FindBy(id = "postcode")
    private WebElement postcode;

    @FindBy(id = "phone")
    private WebElement home_phone;

    @FindBy(id = "phone_mobile")
    private WebElement phone_mobile;

    public void clearTextWhichPersonalAndAddressFields() {
        firstName.clear();
        lastName.clear();
        address1.clear();
        address2.clear();
        city.clear();
    }

    public void clearAllTextInFields(RandomUser user) {
        firstName.clear();
        lastName.clear();
        address1.clear();
        address2.clear();
        city.clear();
        Select stateSelect = new Select(this.id_state);
        stateSelect.selectByIndex(0);
        postcode.clear();
        home_phone.clear();
        phone_mobile.clear();
    }

    public void submitAddress() {
        submitAddressButton.sendKeys(Keys.ENTER);
    }

    public String getChangeAddressErrorMainText() {
        wait.until(ExpectedConditions.visibilityOf(changeAddressAlertMainText));
        return changeAddressAlertMainText.getText();
    }

    public String getChangeAddressErrorDetailsText() {
        wait.until(ExpectedConditions.visibilityOf(changeAddressAlertDetailsText));
        return changeAddressAlertDetailsText.getText();
    }

    public void putPersonalAndAddressDate(RandomUser user) {
        firstName.sendKeys(user.firstName);
        lastName.sendKeys(user.lastName);
        this.address1.sendKeys(user.address1);
        this.city.sendKeys(user.city);
    }

    public void putPersonalAndAllAddressDate(RandomUser user) {
        firstName.sendKeys(user.firstName);
        lastName.sendKeys(user.lastName);
        this.address1.sendKeys(user.address1);
        this.city.sendKeys(user.city);
        Select stateSelect = new Select(this.id_state);
        stateSelect.selectByVisibleText(user.state);
        this.postcode.sendKeys(user.postcode);
        this.phone_mobile.sendKeys(user.phone);
    }

    public void clearCompanyAndPutCompany1(){
        company.clear();
        company.sendKeys("Company1");
    }
}

















