package pageobjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.RandomUser;

import java.util.List;
import java.util.Random;

public class MyAddressesPage extends BasePage {


    public MyAddressesPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(xpath = "//a[@title  = 'Add an address']")
    WebElement addAnAddressButton;

    @FindBy(xpath = "//a[@title = 'Update']")
    WebElement updateButton;

    @FindBy(id = "address_name")
    private WebElement address_name;

    @FindBy(className = "page-heading")
    private WebElement page_headingMyAddresses;

    @FindBy(className = "navigation_page")
    WebElement orderHistoryText;

    @FindBy(xpath = "//a[@title = 'Add an address']")
    WebElement addNewAddressButton;

    @FindBy(id = "submitAddress")
    private WebElement submitAddressButton;

    @FindBy(id = "address1")
    private WebElement address1;

    @FindBy(id = "address2")
    private WebElement address2;

    @FindBy(id = "city")
    private WebElement city;

    @FindBy(id = "id_state")
    private WebElement id_state;

    @FindBy(id = "postcode")
    private WebElement postcode;

    @FindBy(id = "phone")
    private WebElement home_phone;

    @FindBy(id = "phone_mobile")
    private WebElement phone_mobile;

    @FindBy(id = "alias")
    private WebElement addressTitle;

    @FindBy(id = "submitAddress")
    private WebElement submitAddress;

    @FindBy(xpath = "//div[@class='alert alert-danger']//p[1]")
    private WebElement newAddressAlertMainText;

    @FindBy(xpath = "//div[@class='alert alert-danger']//ol")
    private WebElement newAddressAlertDetailsText;

    @FindBy(xpath = "//ul[@class='first_item item box']")
    private WebElement firstAddressDetails;

    @FindBy(xpath = "//a[@title = 'Delete']")
    private WebElement deleteAddressButton;

     @FindBy(css = ".address_update")
    private List<WebElement> listOfDeleteAddressButton;

    @FindBy(css = ".page-subheading")
    private List<WebElement> addressTitles;

    @FindBy(css = ".address")
    private List<WebElement> addressBoxes;

    public boolean isAddAnAddressButtonDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(addAnAddressButton));
        return addAnAddressButton.isDisplayed();
    }

    public ChangeAddressesPage goToUpdateAddressesPage() {
        wait.until(ExpectedConditions.elementToBeClickable(updateButton));
        updateButton.click();
        return new ChangeAddressesPage(driver, wait);
    }

    public String getPageHeadingText() {
        wait.until(ExpectedConditions.visibilityOf(page_headingMyAddresses));
        return page_headingMyAddresses.getText();
    }

    public String getOrderHistoryText() {
        wait.until(ExpectedConditions.visibilityOf(orderHistoryText));
        return orderHistoryText.getText();
    }

    public ChangeAddressesPage goToAddNewAddress() {
        wait.until(ExpectedConditions.elementToBeClickable(addNewAddressButton));
        addNewAddressButton.click();
        return new ChangeAddressesPage(driver, wait);
    }

    public String putAllAddressDateAndClearAddressTitle(RandomUser user) {
        this.address1.sendKeys(user.address1);
        this.city.sendKeys(user.city);
        Select stateSelect = new Select(this.id_state);
        stateSelect.selectByVisibleText(user.state);
        this.postcode.sendKeys(user.postcode);
        this.phone_mobile.sendKeys(user.phone);
        addressTitle.clear(); //czyszczenie pola
        Random random = new Random();
        String titleId = (user.city + ":" + random.nextInt(99));
        addressTitle.sendKeys(titleId);
        submitAddress.sendKeys(Keys.ENTER);
        return titleId;
    }

    public void submitAddress() {
        submitAddress.sendKeys(Keys.ENTER);
    }

    public String getNewAddressErrorMainText() {
        wait.until(ExpectedConditions.visibilityOf(newAddressAlertMainText));
        return newAddressAlertMainText.getText();
    }

    public String getNewAddressErrorDetailsText() {
        wait.until(ExpectedConditions.visibilityOf(newAddressAlertDetailsText));
        return newAddressAlertDetailsText.getText();
    }

    public String getFirstAddressDetails() {
        wait.until(ExpectedConditions.visibilityOf(firstAddressDetails));
        return firstAddressDetails.getText();
    }

    public void clickOnDeleteButton() {
        wait.until(ExpectedConditions.elementToBeClickable(deleteAddressButton));
        deleteAddressButton.click();
        driver.switchTo().alert().accept();
        wait.until(ExpectedConditions.visibilityOf(addNewAddressButton));
        driver.navigate().refresh();
    }

    public int getNumberOfDeleteButton() {
        wait.until(ExpectedConditions.visibilityOf(addNewAddressButton));
        return listOfDeleteAddressButton.size();
    }

    private void scrollToElement(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public boolean addressDataMatch(String addressTitleId, RandomUser userData) {
        int index = -1;
        System.out.println(addressTitleId);
        for (int i = 0; i < addressTitles.size(); i++) {
            System.out.println(addressTitles.get(i).getText());
            if (addressTitles.get(i).getText().equals(addressTitleId.toUpperCase()))
                index = i;
        }
        System.out.println("index: " + index);
        String addressFullText = addressBoxes.get(index).getText();
        System.out.println(addressFullText);
        Boolean dataMatched;
        if (
                addressFullText.contains("ludmila")
                && addressFullText.contains("Test")
                && addressFullText.contains(userData.postcode)
                && addressFullText.contains(userData.address1)
                && addressFullText.contains(userData.city)
                && addressFullText.contains(userData.state)
                && addressFullText.contains(userData.phone)) {
            dataMatched = true;
        } else {
            dataMatched = false;
            System.out.println("Address data mismatch");
            System.out.println("Address data: " + addressFullText);
            System.out.println("User data: " + userData);
        }
        return dataMatched;
    }
}
