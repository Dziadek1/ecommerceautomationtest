package pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {
    @FindBy(id = "email")
    private WebElement loginField;

    @FindBy(id = "passwd")
    private WebElement passwordField;

    @FindBy(id = "email_create")
    private WebElement email_create;

    @FindBy(id = "SubmitCreate")
    private WebElement submitCreate;

    @FindBy(className = "alert-danger")
    private WebElement authAlert;

    @FindBy(linkText = "Forgot your password?")
    private WebElement forgotPasswordLink;

    @FindBy(id = "SubmitLogin")
    private WebElement submitLoginButton;

    @FindBy(xpath = "//h1[@class = 'page-heading']")
    private WebElement MyAccountButton;

    @FindBy(id = "email_create")
    private WebElement emailField;

    public LoginPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void login(String email, String password) {
        loginField.sendKeys(email);
        passwordField.sendKeys(password);
        submitLoginButton.click();
    }

    public RegisterPage goToRegister(String email) {
        email_create.sendKeys(email);
        submitCreate.click();
        return new RegisterPage(email, driver, wait);
    }

    public boolean isEmailRequiredAlertDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(authAlert));
        return authAlert.getText().contains("An email address required");
    }

    public boolean isInvalidEmailAlertDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(authAlert));
        return authAlert.getText().contains("Invalid email address");
    }

    public boolean isPasswordIsRequiredAlertDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(authAlert));
        return authAlert.getText().contains("Password is required");
    }

    public boolean isAuthenticationFailedAlertDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(authAlert));
        return authAlert.getText().contains("Authentication failed");
    }

    public ForgotPasswordPage goToForgotPasswordPage() {
        wait.until(ExpectedConditions.elementToBeClickable(forgotPasswordLink));
        forgotPasswordLink.click();
        return new ForgotPasswordPage(driver, wait);
    }

    public MyAccountPage goMyAccount() {
        wait.until(ExpectedConditions.elementToBeClickable(MyAccountButton));
        MyAccountButton.click();
        return new MyAccountPage(driver, wait);
    }
}
