package pageobjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SearchResultsPage extends BasePage {

    public SearchResultsPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(xpath = "//span[@class='heading-counter']")
    private WebElement searchResultsSummaryText;

    @FindBy(xpath = "//div[@class='product-container']")
    private List<WebElement> searchResults;

    @FindBy(xpath = "//div[@class='product-container']")
    private List<WebElement> products;

    @FindBy(xpath = "//a[contains(@class,'ajax_add_to_cart_button')]")
    private List<WebElement> addToCartButtons;

    @FindBy(xpath = "//span[@title='Continue shopping']")
    //@FindBy(xpath = "//i[@class = 'icon-chevron-left left']")
    private WebElement continueShoppingButton;

    @FindBy(xpath = "//div[@class='shopping_cart']//b")
    private WebElement cartIcon;

    @FindBy(id = "button_order_cart")
    private WebElement placeOrderButton;

    @FindBy(xpath = "//p[@class = 'cart_navigation clearfix']//a[@title='Proceed to checkout']")
    private WebElement proceedToCheckoutButton;

    @FindBy(xpath = "//p[@class='cart_navigation clearfix']/button[@type='submit']")
    private WebElement nextStepsProceedButton;

    @FindBy(xpath = "//button[@name='processCarrier']")
    private WebElement processOrderButton;

    @FindBy(id = "cgv")
    private WebElement termsCheckbox;

    @FindBy(xpath = "//a[@class='bankwire']")
    private WebElement payByBankWireButton;

    @FindBy(xpath = "//div[@class='box']")
    private WebElement orderDetailsBox;

    @FindBy(className = "page-heading")
    private WebElement pageHeadingText;

    @FindBy(xpath = "//span[@class = 'price']")
    private WebElement priceInOrderConfirmation;

    @FindBy(className = "compare-form")
    private WebElement compareButton;

    public String getSearchResultsSummaryText() {
        wait.until(ExpectedConditions.visibilityOf(searchResultsSummaryText));
        return searchResultsSummaryText.getText();
    }

    public int getSearchResultsProductsNumber() {
        wait.until(ExpectedConditions.visibilityOf(searchResultsSummaryText));
        return searchResults.size();
    }

    public void addToCartAndProccedCheckOut(int index) {
        scrollToElement(compareButton);
        scrollToElement(products.get(index));
        wait.until(ExpectedConditions.visibilityOf(products.get(index)));
        Actions builder = new Actions(driver);
        builder.moveToElement(products.get(index)).moveToElement(products.get(index)).build().perform();
        wait.until(ExpectedConditions.elementToBeClickable(addToCartButtons.get(index)));
        addToCartButtons.get(index).click();
        wait.until(ExpectedConditions.elementToBeClickable(continueShoppingButton));
        continueShoppingButton.click();
        scrollToElement(cartIcon);
        builder.moveToElement(cartIcon).build().perform();
        wait.until(ExpectedConditions.elementToBeClickable(placeOrderButton));
        placeOrderButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButton));
        proceedToCheckoutButton.click();
    }

    private void scrollToElement(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public String getPageHeadingText() {
        wait.until(ExpectedConditions.visibilityOf(pageHeadingText));
        return pageHeadingText.getText();
    }
    public String getPriceInOrderConfirmation()  {
        wait.until(ExpectedConditions.visibilityOf(priceInOrderConfirmation));
        return priceInOrderConfirmation.getText();
    }
}
