package pageobjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.Collection;
public class OrderHistoryPage extends BasePage{
    public OrderHistoryPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }
    @FindBy(className = "navigation_page")
    WebElement orderHistoryText;
    public String getOrderHistoryText() {
        wait.until(ExpectedConditions.visibilityOf(orderHistoryText));
        return orderHistoryText.getText();
    }
}