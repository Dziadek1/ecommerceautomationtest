package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

public class ShoppingPage extends BasePage {

    private final String CATEGORY_PAGE_URL = "http://automationpractice.com/index.php?id_category=3&controller=category";

    private double expectedBasketValue = 2; //add shopping cost at start
    Random random = new Random();

    public ShoppingPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void open() {
        driver.get(CATEGORY_PAGE_URL);
    }

    @FindBy(xpath = "//div[@class='product-container']")
    private List<WebElement> products;

    @FindBy(xpath = "//span[@title='Continue shopping']")
    private WebElement continueShoppingButton;

    @FindBy(xpath = "//a[contains(@class,'ajax_add_to_cart_button')]")
    private List<WebElement> addToCartButtons;

    @FindBy(xpath = "//div[@class='right-block']//span[@class='price product-price']")
    private List<WebElement> productsPrices;

    @FindBy(xpath = "//span[contains(@class,'cart_block_total')]")
    private WebElement currentBasketTotal;

    @FindBy(xpath = "//div[@class='shopping_cart']//b")
    private WebElement cartIcon;

    @FindBy(id = "button_order_cart")
    private WebElement placeOrderButton;

    @FindBy(xpath = "//button[@name = 'processAddress']//span")
    private WebElement proceedToCheckout;

    @FindBy(xpath = "//p[@class='cart_navigation clearfix']//a[@title='Proceed to checkout']")
    private WebElement firstStepProceedButton;

    @FindBy(xpath = "//p[@class='cart_navigation clearfix']/button[@type='submit']")
    private WebElement nextStepsProceedButton;

    @FindBy(xpath = "//button[@name='processCarrier']")
    private WebElement processOrderButton;

    @FindBy(id = "cgv")
    private WebElement termsCheckbox;

    @FindBy(xpath = "//a[@class='bankwire']")
    private WebElement payByBankWireButton;

    @FindBy(xpath = "//div[@class='box']")
    private WebElement orderDetailsBox;

    @FindBy(className = "form-control")
    private WebElement formControlField;


    public void addRandomProductsToTheBasket(int productNumber) {
        for (int i = 0; i < productNumber; i++) {
            int randomProductIndex = random.nextInt(products.size() - 1);
            updateExpectedBasketValue(randomProductIndex);
            moveMouseToProduct(randomProductIndex);
            clickOnAddToCartButton(randomProductIndex);
            clickOnContinueShoppingButton();
        }
    }

    private void updateExpectedBasketValue(int productIndex) {
        wait.until(ExpectedConditions.visibilityOf(productsPrices.get(productIndex)));
        String productPrice = productsPrices.get(productIndex).getText();//$12.12
        productPrice = productPrice.substring(1, productPrice.length());//12.12
        double price = Double.parseDouble(productPrice);
        expectedBasketValue += price;
    }

    private void clickOnContinueShoppingButton() {
        wait.until(ExpectedConditions.elementToBeClickable(continueShoppingButton));
        continueShoppingButton.click();
    }

    private void clickOnAddToCartButton(int index) {
        wait.until(ExpectedConditions.visibilityOf(addToCartButtons.get(index)));
        addToCartButtons.get(index).click();
    }

    private void moveMouseToProduct(int index) {
        scrollToElement(products.get(index));
        wait.until(ExpectedConditions.visibilityOf(products.get(index)));
        Actions builder = new Actions(driver);
        builder.moveToElement(products.get(index)).moveToElement(products.get(index)).build().perform();
    }

    public double getExpectedBasketValue() {
        double roundOffValue = Math.round(expectedBasketValue * 100.0) / 100.0;
        return roundOffValue;
    }

    public double getCurrentBasketValue() {
        scrollToElement(cartIcon);
        Actions builder = new Actions(driver);
        builder.moveToElement(cartIcon).build().perform();
        wait.until(ExpectedConditions.visibilityOf(currentBasketTotal));
        return Double.parseDouble(currentBasketTotal.getText().substring(1, currentBasketTotal.getText().length()));
    }

    public void placeOrderForCurrentBasket() {
        clickOnBasketPlaceOrderButton();
        proccedToPayment();
        payByBankWireAndConfirmOrder();
    }

    public void placeOrderForCurrentBasketAfterLogin() {
        proceedToCheckout();
        termsCheckbox.click();
        wait.until(ExpectedConditions.elementToBeClickable(processOrderButton));
        processOrderButton.click();
        payByBankWireAndConfirmOrder();
    }

    private void payByBankWireAndConfirmOrder() {
        wait.until(ExpectedConditions.elementToBeClickable(payByBankWireButton));
        payByBankWireButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(nextStepsProceedButton));
        nextStepsProceedButton.click();
    }

    private void proccedToPayment() {
        wait.until(ExpectedConditions.elementToBeClickable(firstStepProceedButton));
        firstStepProceedButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(nextStepsProceedButton));
        nextStepsProceedButton.click();
        termsCheckbox.click();
        wait.until(ExpectedConditions.elementToBeClickable(processOrderButton));
        processOrderButton.click();
    }

    private void clickOnBasketPlaceOrderButton() {
        scrollToElement(cartIcon);
        Actions builder = new Actions(driver);
        builder.moveToElement(cartIcon).build().perform();
        scrollToElement(formControlField);
        wait.until(ExpectedConditions.visibilityOf(placeOrderButton));
        placeOrderButton.click();
    }
    private void proceedToCheckout() {
        scrollToElement(cartIcon);
        Actions builder = new Actions(driver);
        builder.moveToElement(cartIcon).build().perform();
        scrollToElement(formControlField);
        formControlField.click();
        wait.until(ExpectedConditions.visibilityOf(proceedToCheckout));
        proceedToCheckout.click();
    }

    public String getOrderDetailsBoxText() {
        wait.until(ExpectedConditions.visibilityOf(orderDetailsBox));
        return orderDetailsBox.getText();
    }

    private void scrollToElement(WebElement element){
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }
}

