package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.*;
import utils.Navigation;

import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MyAccountTest extends BaseTest{
    private static final String TITLE = "My account - My Store";

    @Test
    void shouldMoveToMyAddressesPage() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("Testludmila@test.pl", "Testludmila@test.pl");
        MyAccountPage myAccountPage = loginPage.goMyAccount();
        MyAddressesPage myAddressesPage = myAccountPage.goToMyAddressesPage();
        Assertions.assertTrue(myAddressesPage.isAddAnAddressButtonDisplayed());
    }

    @Test
    void shouldMoveToOrderHistoryPage() {
        MyAccountPage myAccountPage = Navigation.goMyAccountPage(driver,wait);
        MyAddressesPage orderHistoryPage = myAccountPage.goToOrderHistoryAndDetailsPage();
        assertTrue(orderHistoryPage.getOrderHistoryText().contains("Order history"));
    }

    @Test
    public void shouldGetCorrectPageTitle(){
        MyAccountPage myAccountPage = Navigation.goMyAccountPage(driver,wait);
        String title = myAccountPage.getTitle();
        assertEquals(TITLE, title);
    }
}
