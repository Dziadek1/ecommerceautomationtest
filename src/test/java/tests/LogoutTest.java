package tests;

import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.LoginPage;
import utils.Navigation;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LogoutTest extends BaseTest{

    @Test
    void shouldLogoutUserWhenSignOutButtonIsUsed() {
        BasePage basePage = Navigation.goToBasePage(driver,wait);
        LoginPage loginPage = basePage.goToLogin();
        loginPage.login("Testludmila@test.pl", "Testludmila@test.pl");
        basePage.clickOnSignOutButton();
        assertTrue(basePage.isSignInButtonDisplayed());
    }
}
