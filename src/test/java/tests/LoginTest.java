package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import utils.Navigation;

public class LoginTest extends BaseTest{

    @Test
    void shouldDisplayInvalidEmailAlertWrongEmailIsProvided() {
        LoginPage loginPage = Navigation.goToLoginPage(driver,wait);
        loginPage.login("test", "wrongpass");
        Assertions.assertTrue(loginPage.isInvalidEmailAlertDisplayed());
    }

    @Test
    void shouldDisplayPasswordIsRequiredWhenNoPasswordIsProvided() {
        LoginPage loginPage = Navigation.goToLoginPage(driver,wait);
        loginPage.login("test@softie.pl", "");
        Assertions.assertTrue(loginPage.isPasswordIsRequiredAlertDisplayed());
    }

    @Test
    void shouldDisplayAuthenticationFailedWhenWrongCredentialsAreProvided() {
        LoginPage loginPage = Navigation.goToLoginPage(driver,wait);
        loginPage.login("Testludmila@test.pl", "wrongPass");
        Assertions.assertTrue(loginPage.isAuthenticationFailedAlertDisplayed());
    }

    @Test
    void shouldDisplayEmailRequiredAlertWhenNoEmailIsProvided() {
        LoginPage loginPage = Navigation.goToLoginPage(driver,wait);
        loginPage.login("", "");
        Assertions.assertTrue(loginPage.isEmailRequiredAlertDisplayed());
    }

    @Test
    void shouldLoginUserWhenCorrectCredentialsAreProvided() {
        LoginPage loginPage = Navigation.goToLoginPage(driver,wait);
        loginPage.login("Testludmila@test.pl", "Testludmila@test.pl");
        Assertions.assertTrue(loginPage.isSignOutButtonDisplayed());
    }
}
