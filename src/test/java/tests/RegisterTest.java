package tests;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import pageobjects.RegisterPage;
import utils.Navigation;
import utils.RandomUser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegisterTest extends BaseTest{

    Faker faker = new Faker();
    String firstName = faker.name().firstName();
    String lastName = faker.name().lastName();


    @Test
    void shouldRegisterUserWhenAllMandatoryDataProvided() {
        HomePage basePage = new HomePage(driver, wait);
        basePage.open();
        LoginPage loginPage = basePage.goToLogin();
        RandomUser randomUser = new RandomUser();
        RegisterPage registerPage = loginPage.goToRegister(randomUser.email);
        registerPage.register(randomUser);
        assertTrue(basePage.isSignOutButtonDisplayed());
    }

    @Test
    void shouldDisplayMissingMandatoryDataErrorWhenIncompleteDataProvided()  {
        LoginPage loginPage = Navigation.goToLoginPage(driver,wait);

        RandomUser randomUser = new RandomUser();
        randomUser.phone = "";
        randomUser.address1 = "";
        randomUser.city = "";
        randomUser.postcode = "";

        RegisterPage registerPage = loginPage.goToRegister(randomUser.email);
        registerPage.register(randomUser);

        assertEquals("There are 4 errors", registerPage.getRegisterErrorMainText());
        assertTrue(registerPage.getRegisterErrorDetailsText().contains("You must register at least one phone number."));
        assertTrue(registerPage.getRegisterErrorDetailsText().contains("address1 is required."));
        assertTrue(registerPage.getRegisterErrorDetailsText().contains("city is required."));
        assertTrue(registerPage.getRegisterErrorDetailsText().contains("The Zip/Postal code you've entered is invalid."));
    }

    @Test
    void shouldDisplayIncorrectPostcodeErrorWhenTooLongPostcodeProvided() {
        LoginPage loginPage = Navigation.goToLoginPage(driver,wait);
        RandomUser randomUser = new RandomUser();
        randomUser.postcode = "7676876";
        RegisterPage registerPage = loginPage.goToRegister(randomUser.email);
        registerPage.register(randomUser);
        assertEquals("The Zip/Postal code you've entered is invalid. It must follow this format: 00000", registerPage.getRegisterErrorDetailsText());
    }

    @Test
    void shouldDisplayMissingMandatoryDataErrorWhenNoDataProvided()  {
        LoginPage loginPage = Navigation.goToLoginPage(driver,wait);
        RandomUser randomUser = new RandomUser();
        RegisterPage registerPage = loginPage.goToRegister(randomUser.email);
        registerPage.clickOnRegisterButton();
        assertEquals("There are 8 errors", registerPage.getRegisterErrorMainText());
        assertTrue(registerPage.getRegisterErrorDetailsText().contains("You must register at least one phone number."));
        assertTrue(registerPage.getRegisterErrorDetailsText().contains("lastname is required."));
        assertTrue(registerPage.getRegisterErrorDetailsText().contains("firstname is required."));
        assertTrue(registerPage.getRegisterErrorDetailsText().contains("passwd is required."));
        assertTrue(registerPage.getRegisterErrorDetailsText().contains("address1 is required."));
        assertTrue(registerPage.getRegisterErrorDetailsText().contains("city is required."));
        assertTrue(registerPage.getRegisterErrorDetailsText().contains("The Zip/Postal code you've entered is invalid."));
        assertTrue(registerPage.getRegisterErrorDetailsText().contains("This country requires you to choose a State."));
    }
}
