package tests;

import org.junit.jupiter.api.Test;
import pageobjects.ForgotPasswordPage;
import utils.Navigation;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ForgotPasswordTest extends BaseTest{

    @Test
    void shouldDisplayInvalidEmailAddressErrorWhenIncorrectSyntaxEmailProvided() {
        ForgotPasswordPage forgotPasswordPage = Navigation.goToForgotPasswordPage(driver,wait);
        forgotPasswordPage.retrievePasswordForEmail("wrong@email@syntax@com");
        assertEquals("There is 1 error", forgotPasswordPage.getErrorMessageMainText());
        assertEquals("Invalid email address.", forgotPasswordPage.getErrorMessageDetailsText());
    }

    @Test
    void shouldDisplayNoAccountRegisteredForThisEmailWhenUnregisteredEmailProvided() {
        ForgotPasswordPage forgotPasswordPage = Navigation.goToForgotPasswordPage(driver,wait);
        forgotPasswordPage.retrievePasswordForEmail("thisemailnotexist@gmail.com");
        assertEquals("There is 1 error", forgotPasswordPage.getErrorMessageMainText());
        assertEquals("There is no account registered for this email address.", forgotPasswordPage.getErrorMessageDetailsText());
    }

    @Test
    void shouldDisplaySuccessAlertWhenRegisteredEmailProvided() {
        ForgotPasswordPage forgotPasswordPage = Navigation.goToForgotPasswordPage(driver,wait);
        forgotPasswordPage.retrievePasswordForEmail("Testludmila@test.pl");
        assertEquals("A confirmation email has been sent to your address: Testludmila@test.pl", forgotPasswordPage.getSuccessAlertText());
    }
}
