package tests;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.*;
import utils.Navigation;
import utils.RandomUser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChangeAddressesTest extends BaseTest{

    @Test
    void shouldChangeAddressesWhenSameMandatoryDataWhereChanged() {
        RandomUser randomUser = new RandomUser();
        MyAddressesPage myAddressesPage = Navigation.goToAddressesPage(driver,wait);
        ChangeAddressesPage changeAddressesPage = myAddressesPage.goToUpdateAddressesPage();
        changeAddressesPage.clearTextWhichPersonalAndAddressFields();
        changeAddressesPage.putPersonalAndAddressDate(randomUser);
        changeAddressesPage.submitAddress();
        assertTrue(myAddressesPage.getPageHeadingText().contains("MY ADDRESSES"));
    }

    @Test
    void shouldChangeAddressesWhenAllMandatoryDataWhereChanged() {
        RandomUser randomUser = new RandomUser();
        MyAddressesPage myAddressesPage = Navigation.goToAddressesPage(driver,wait);
        ChangeAddressesPage changeAddressesPage = myAddressesPage.goToUpdateAddressesPage();
        changeAddressesPage.clearAllTextInFields(randomUser);
        changeAddressesPage.putPersonalAndAllAddressDate(randomUser);
        changeAddressesPage.submitAddress();
        assertTrue(myAddressesPage.getPageHeadingText().contains("MY ADDRESSES"));
    }

    @Test
    void shouldGoToMyAccountPageWhenNoChangeWhereDone() {
        MyAddressesPage myAddressesPage = Navigation.goToAddressesPage(driver,wait);
        ChangeAddressesPage changeAddressesPage = myAddressesPage.goToUpdateAddressesPage();
        changeAddressesPage.submitAddress();
        assertTrue(myAddressesPage.getPageHeadingText().contains("MY ADDRESSES"));
    }

    @Test
    void shouldDisplayMissingMandatoryDataErrorWhenSameIncompleteDataProvided() {
        ChangeAddressesPage changeAddressesPage = Navigation.goToChangeAddressesPage(driver,wait);
        changeAddressesPage.clearTextWhichPersonalAndAddressFields();
        changeAddressesPage.submitAddress();
        assertEquals("There are 4 errors", changeAddressesPage.getChangeAddressErrorMainText());
        assertTrue(changeAddressesPage.getChangeAddressErrorDetailsText().contains("firstname is required."));
        assertTrue(changeAddressesPage.getChangeAddressErrorDetailsText().contains("lastname is required."));
        assertTrue(changeAddressesPage.getChangeAddressErrorDetailsText().contains("address1 is required."));
        assertTrue(changeAddressesPage.getChangeAddressErrorDetailsText().contains("city is required."));
    }

    @Test
    void shouldDisplayMissingMandatoryDataErrorWhenAllIncompleteDataProvided() {
        RandomUser randomUser = new RandomUser();
        ChangeAddressesPage changeAddressesPage = Navigation.goToChangeAddressesPage(driver,wait);
        changeAddressesPage.clearAllTextInFields(randomUser);
        changeAddressesPage.submitAddress();
        assertEquals("There are 7 errors", changeAddressesPage.getChangeAddressErrorMainText());
        assertTrue(changeAddressesPage.getChangeAddressErrorDetailsText().contains("firstname is required."));
        assertTrue(changeAddressesPage.getChangeAddressErrorDetailsText().contains("lastname is required."));
        assertTrue(changeAddressesPage.getChangeAddressErrorDetailsText().contains("address1 is required."));
        assertTrue(changeAddressesPage.getChangeAddressErrorDetailsText().contains("city is required."));
        assertTrue(changeAddressesPage.getChangeAddressErrorDetailsText().contains("You must register at least one phone number."));
        assertTrue(changeAddressesPage.getChangeAddressErrorDetailsText().contains("This country requires you to chose a State."));
        assertTrue(changeAddressesPage.getChangeAddressErrorDetailsText().contains("The Zip/Postal code you've entered is invalid. It must follow this format: 00000"));
    }
    @Test
    void shouldReturnCorrectCompanyNameAfterChange()  {
        MyAddressesPage myAddressesPage = Navigation.goToAddressesPage(driver,wait);
        ChangeAddressesPage changeAddressesPage = myAddressesPage.goToUpdateAddressesPage();
        changeAddressesPage.clearCompanyAndPutCompany1();
        changeAddressesPage.submitAddress();
        assertTrue(myAddressesPage.getFirstAddressDetails().contains("Company1"));
    }
}
