package tests;

import org.junit.jupiter.api.Test;
import pageobjects.*;
import utils.Navigation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ShoppingTest extends BaseTest {

    @Test
    void shouldCorrectlyUpdateBasketValueWhenAddingProducts() {
        ShoppingPage shoppingPage = new ShoppingPage(driver, wait);
        shoppingPage.open();
        shoppingPage.addRandomProductsToTheBasket(3);
        assertEquals(shoppingPage.getExpectedBasketValue(), shoppingPage.getCurrentBasketValue());
    }

    @Test
    void shouldPlaceAnOrderForAddedProducts() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();

        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("Testludmila@test.pl", "Testludmila@test.pl");

        ShoppingPage shoppingPage = loginPage.goShoppingPage();
        shoppingPage.addRandomProductsToTheBasket(3);
        shoppingPage.placeOrderForCurrentBasket();
        assertTrue(shoppingPage.getOrderDetailsBoxText().contains("$" + shoppingPage.getExpectedBasketValue()));
    }
    @Test
    void shouldCorrectlyBuyBlouse() {
        BasePage basePage = Navigation.goToBasePage(driver,wait);
        SearchResultsPage searchResultsPage = basePage.searchFor("Blouse");
        searchResultsPage.addToCartAndProccedCheckOut(0);
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.login("Testludmila@test.pl", "Testludmila@test.pl");
        ShoppingPage shoppingPage = new ShoppingPage(driver, wait);
        shoppingPage.placeOrderForCurrentBasketAfterLogin();
        assertEquals("$29.00", searchResultsPage.getPriceInOrderConfirmation());
        assertEquals("ORDER CONFIRMATION", searchResultsPage.getPageHeadingText());
    }
}