package tests;

import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.SearchResultsPage;
import utils.Navigation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SearchResultsTest extends BaseTest{

    @Test
    void shouldReturnNonEmptySearchResultsWhenLookingForExistingProduct() {
        BasePage basePage = Navigation.goToBasePage(driver,wait);
        SearchResultsPage searchResultsPage = basePage.searchFor("Dress");
        assertTrue(searchResultsPage.getSearchResultsProductsNumber() > 0);
    }

    @Test
    void shouldReturnEmptySearchResultsWhenLookingForNonExistingProduct() {
        BasePage basePage = Navigation.goToBasePage(driver,wait);
        SearchResultsPage searchResultsPage = basePage.searchFor("Game");
        assertEquals("0 results have been found.", searchResultsPage.getSearchResultsSummaryText());
        assertTrue(searchResultsPage.getSearchResultsProductsNumber() == 0);
    }

    @Test
    void shouldReturnEmptySearchResultsWhenLookingForExistingProductTshirt() {
        BasePage basePage = Navigation.goToBasePage(driver,wait);
        SearchResultsPage searchResultsPage = basePage.searchFor("t-shirt");
        assertEquals("1 result has been found.", searchResultsPage.getSearchResultsSummaryText());
        assertTrue(searchResultsPage.getSearchResultsProductsNumber() == 1);
    }
}
