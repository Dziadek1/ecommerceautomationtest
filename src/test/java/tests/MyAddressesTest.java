package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.*;
import utils.Navigation;
import utils.RandomUser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MyAddressesTest extends BaseTest {
    @Test
    void shouldMoveToMyAddressesPage() {
        MyAddressesPage myAddressesPage = Navigation.goToAddressesPage(driver,wait);
        Assertions.assertTrue(myAddressesPage.isAddAnAddressButtonDisplayed());
    }

    @Test
    void shouldAddNewAddressWhenAllMandatoryDataProvided() {
        MyAddressesPage myAddressesPage = Navigation.goToAddressesPage(driver,wait);
        int quantityOfDeleteButtonBeforeAddedAddress = myAddressesPage.getNumberOfDeleteButton();
        myAddressesPage.goToAddNewAddress();
        RandomUser randomUser = new RandomUser();
        myAddressesPage.putAllAddressDateAndClearAddressTitle(randomUser);
        assertTrue(myAddressesPage.getPageHeadingText().contains("MY ADDRESSES"));
        assertEquals(quantityOfDeleteButtonBeforeAddedAddress + 1, myAddressesPage.getNumberOfDeleteButton());
    }

    @Test
    void shouldDisplayMissingMandatoryDataErrorWhenAllIncompleteDataProvided() {
        MyAddressesPage myAddressesPage = Navigation.goToAddressesPage(driver,wait);
        myAddressesPage.goToAddNewAddress();
        myAddressesPage.submitAddress();
        assertEquals("There are 5 errors", myAddressesPage.getNewAddressErrorMainText());
        assertTrue(myAddressesPage.getNewAddressErrorDetailsText().contains("address1 is required."));
        assertTrue(myAddressesPage.getNewAddressErrorDetailsText().contains("city is required."));
        assertTrue(myAddressesPage.getNewAddressErrorDetailsText().contains("You must register at least one phone number."));
        assertTrue(myAddressesPage.getNewAddressErrorDetailsText().contains("This country requires you to chose a State."));
        assertTrue(myAddressesPage.getNewAddressErrorDetailsText().contains("The Zip/Postal code you've entered is invalid. It must follow this format: 00000"));
     }

    @Test
    void shouldDeleteAddressWhenDeleteButtonClicked() {
        MyAddressesPage myAddressesPage = Navigation.goToAddressesPage(driver,wait);
        myAddressesPage.goToAddNewAddress();
        RandomUser randomUser = new RandomUser();
        myAddressesPage.putAllAddressDateAndClearAddressTitle(randomUser);
        int quantityOfDeleteButtonBeforeAddedAddress = myAddressesPage.getNumberOfDeleteButton();
        myAddressesPage.clickOnDeleteButton();
        assertEquals(quantityOfDeleteButtonBeforeAddedAddress - 1, myAddressesPage.getNumberOfDeleteButton());
    }

    @Test
    void shouldSaveCorrectDataAfterNewAddressAdded() {
        MyAddressesPage myAddressesPage = Navigation.goToAddressesPage(driver, wait);
        int quantityOfDeleteButtonBeforeAddedAddress = myAddressesPage.getNumberOfDeleteButton();
        myAddressesPage.goToAddNewAddress();
        RandomUser randomUser = new RandomUser();
        String newAddressTitle = myAddressesPage.putAllAddressDateAndClearAddressTitle(randomUser);
        assertTrue(myAddressesPage.getPageHeadingText().contains("MY ADDRESSES"));
        assertEquals(quantityOfDeleteButtonBeforeAddedAddress + 1, myAddressesPage.getNumberOfDeleteButton());
        assertTrue(myAddressesPage.addressDataMatch(newAddressTitle, randomUser));
    }
}
