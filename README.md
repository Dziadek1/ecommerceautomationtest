# Selenium WebDriver tests for My Store #

This is project containing automated tests for the eCommerce page www.automationpractice.com.

### Technologies used: ###

* Java 11+
* Maven
* Selenium WebDriver
* jUnit 5.x
* Page object pattern
* Page factory

### To Run Tests: ###

* add to Maven Surefire Plugin
* or in IntelliJ IDEA run test form src\test\java\tests folder


* Java 11+
* Maven
* Selenium WebDriver
* jUnit 5.x
* Page object pattern
* Page factory

### To Run Tests: ###


* it works in three modes: Chrome, Firefox and headless. To change browser you have put browse name in "prepareDriver" method in BaseTest class.
* you can start form IntelliJ IDEA running test form src\test\java\tests folder
* or use console - for example: mvn -Dtest=LoginTest test

### Project structure ###

* Package „pageobjects” contains locations of WebElements and the methods on this WebElements. 
   They are grouped into classes corresponding to the website page.
* Package „tests” contains class with tests for: login, logout, forgot password, register, search results, shopping, add new  and change addresses.

### What is this repository for? ###

* The tests are a part of the final project of the course for automated testers.




